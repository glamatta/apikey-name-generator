# apikey-name-generator

Este es un HTML básico que ejecuta un script local para ayudar a armar los nombre correctos para los apikeys según las definiciones que tenemos como equipo

## ¿Como se usa?

### Opción 1
se descarga el archivo [generator.html](https://gitlab.com/cencosud-ds/cencommerce/buying-experience/payments/utilities/apikey-name-generator/-/raw/master/generator.html?inline=false) y se abre con cualquier browser (para que se pueda ejecutar el javascript que tiene)

### Opción 2
Clonar el repo y ejecutar:

```
npm install
```
Para instalar la aplicación versión electron (sólo la primera vez es necesario, si no están aún instaladas las dependencias)

y luego para iniciarla

```
npm start
```